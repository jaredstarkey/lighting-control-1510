#!/bin/bash

apt-get install -y expect


if [ ! -d "/home/dari/altitude" ]; then
	mkdir /home/dari/altitude
	chown dari:dari /home/dari/altitude
fi

if [ ! -d "/home/dari/altitude/scripts" ]; then
	mkdir /home/dari/altitude/scripts
	chown dari:dari /home/dari/altitude
fi

cp ./*.sh /home/dari/altitude/scripts/
cp ./*.expect /home/dari/altitude/scripts/
cp ./*.desktop /home/dari/Desktop/
chown dari:dari /home/dari/altitude/scripts/*
chown dari:dari /home/dari/Desktop/*.desktop

chmod +x /home/dari/Desktop/*.desktop
chmod +x /home/dari/altitude/scripts/*

echo "dari ALL=NOPASSWD:/home/dari/altitude/scripts/*, /usr/sbin/service" >> /etc/sudoers
